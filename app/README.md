
# Sawit Gallery
This Project focus on code, there is no time to hardening docker 
## Tech Stack

**App:** Codeigniter, Bootstrap CSS

**Database:** Mysql

**Container:** Docker


## Features

- Login
- Register
- Gallery


## Preparation

This Project is using docker so need to install docker and docker-compose 

Installing Docker and Docker Compose
Here's a breakdown of installing Docker and Docker Compose:

1. Install Docker:

Follow the official Docker installation guide for your operating system: https://docs.docker.com/engine/install/
The guide provides instructions for Windows, macOS, and Linux distributions.

2. Install Docker Compose:

Once Docker is installed, follow the official guide to install Docker Compose: https://docs.docker.com/compose/install/
The guide offers instructions based on your operating system (mentioned above).
Additional Notes:

You might require administrative privileges during the installation process.
After installation, verify Docker and Docker Compose are working by running docker version and docker-compose version commands in your terminal.
I hope this helps!
    
## Requirement Checklist

- [x] User Registration & Login 
- [x] Secure Image Upload
- [x] Image Storage
- [x] Image Retrieval
- [x] Image Deletion
- [x] User Feedback

You Can Check the detail here is testing based on requirement ddd.pdf 

## Getting Started

To run this project

```bash
  docker-compose up --build 
```

if you got some error try using administrator or root privilege

```bash
  sudo docker-compose up --build
```

if the script is running then you can access the app 

```bash
  http://localhost:8080
```
if the port is conflict and you want to customize you can check and edit in docker-compose.yml
edit in this part

```bash
  ports:
      - "8080:80"
```
