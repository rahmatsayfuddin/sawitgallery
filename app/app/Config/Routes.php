<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'UserController::index');
$routes->get('/register', 'UserController::index');
$routes->post('/register', 'UserController::register');
$routes->get('/login', 'UserController::loginUI');
$routes->post('/login', 'UserController::login');
$routes->get('/logout', 'UserController::logout');
$routes->get('/gallery', 'GalleryController::index');
$routes->post('/gallery/upload', 'GalleryController::upload');
$routes->get('/gallery/full/(:any)', 'GalleryController::getFullImage/$1');
$routes->get('gallery/index/(:num)', 'GalleryController::index/$1');
$routes->get('gallery/delete/(:any)', 'GalleryController::delete/$1');

