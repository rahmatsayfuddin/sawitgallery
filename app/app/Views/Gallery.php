<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<div>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Sawit Gallery</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#">Gallery</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url().'/logout/'; ?>">Logout</a>
        </li>  
      </ul>
    </div>
  </div>
</nav>

<div class="container mt-3">
<?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error'); ?>
                </div>
            <?php endif; ?>

<div class="container mt-3">
  <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">
  Upload an Image
  </button>
</div>
<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog  modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Upload Image</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <form action="<?php echo base_url('gallery/upload'); ?>" method="post" enctype="multipart/form-data" class="form-group">
        <div class="mb-3">
            <label for="image" class="form-label">Choose Image:</label>
            <input type="file" class="form-control" name="image" id="image">
        </div>
        <div class="mb-3">
            <label for="caption" class="form-label">Image Caption:</label>
            <input type="text" class="form-control" name="caption" id="caption" placeholder="Enter a caption for the image">
        </div>
        <button type="submit" class="btn btn-primary">Upload</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<div class="row">
<div class="container mt-3">
<ul class="pagination">
 <?php echo $pagination ;?>
</ul>
</div>
<?php
foreach ($images as $image) {?>
<div class="col-md-4">
  <div class="card" >
    <img class="card-img-top" src="<?php echo $image['base64'];?>" alt="Card image" style="width:100%">
    <div class="card-body">
      <p class="card-text"><?php echo htmlentities($image['caption']);?></p>
      <a href="<?php echo base_url().'/gallery/full/'.$image['imgid']; ?>" class="btn btn-primary">View Image</a>
      <a href="<?php echo base_url().'/gallery/delete/'.$image['imgid']; ?>" class="btn btn-danger">Delete Image</a>
    </div>
  </div>
  </div>
  <?php } ?>
  </div>
</div>

</body>
<style>
  .card-img-top, .card-img-bottom {
    height: 200px; /* Adjust height as needed */
    object-fit: cover; /* Maintain aspect ratio and resize to fit */
  }
</style>
</html>
