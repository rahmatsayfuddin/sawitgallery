<!doctype html>
<html lang="en" class="h-100">

<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</head>

<body class="d-flex flex-column h-100">

    <!-- Begin page content -->
    <main class="flex-shrink-0">
        <div class="container">
            <h1 class="mt-5">Login Form</h1>
            <hr />
            <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error'); ?>
                </div>
            <?php endif; ?>
            <form method="post" action="<?= base_url(); ?>login">
                <div class="mb-3 mt-3">
                        <label for="username"  class="form-label">Username:</label>
                        <input type="text" id="username" class="form-control" name="username">
                </div>
                <div class="mb-3">
                        <label for="password"  class="form-label">Password:</label>
                        <input type="password" id="password" class="form-control" name="password">
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
                <p class="text-center mt-3">
                    Don't have an account? <a href="<?= base_url(); ?>register">Register Here</a>
                    </p>
            </form>
            <hr />
        </div>
    </main>



</body>

</html>