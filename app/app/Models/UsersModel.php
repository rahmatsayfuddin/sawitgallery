<?php

namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\HTTP\IncomingRequest;

class UsersModel extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'uuid';
    protected $returnType = 'array';
    protected $allowedFields = ['uuid','username', 'password'];

    protected $beforeInsert = ['hashPassword'];
    protected $ipAddress;
    protected function hashPassword(array $data)
    {
        if (!isset($data['data']['password'])) {
            return $data;
        }

        $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_ARGON2I);
        return $data;
    }
    public function getUserByUsername($username)
    {
        return $this->where('username', $username)->first();
    }

}
