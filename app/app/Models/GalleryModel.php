<?php

namespace App\Models;

use CodeIgniter\Model;

class GalleryModel extends Model
{
    protected $table = 'gallery';
    protected $allowedFields = ['imgid','uuid', 'caption', 'base64'];

    public function saveImage($data)
    {
        return $this->insert($data);
    }

    public function getImages($currentPage = 1, $limit = 10,$uuid)
    {
        $offset = ($currentPage - 1) * $limit;
        $builder = $this->builder();
        return $builder->select('*')
                      ->limit($limit)
                      ->offset($offset)
                      ->where('uuid', $uuid)
                      ->get()->getResultArray();
    }

    public function countImages()
    {
        $builder = $this->builder();
        return $builder->countAllResults();
    }

    public function getImageById(string $imageId, string $uuid)
    {
        $builder = $this->db->table($this->table);
        $builder->where('imgid', $imageId);
        $builder->where('uuid', $uuid);
        $query = $builder->get();

        // Handle successful or unsuccessful query results
        if ($query->getNumRows() === 1) {
            return $query->getRow();
        } else {
            return null;
        }
    }

        public function deleteImage(string $imageId, string $uuid)
    {
        $builder = $this->db->table($this->table); 
        $builder->where('imgid', $imageId);
        $builder->where('uuid', $uuid);
        $query = $builder->delete();
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
