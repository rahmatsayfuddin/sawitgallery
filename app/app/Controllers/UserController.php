<?php

namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\Config\Services;
use CodeIgniter\Controller;
use CodeIgniter\Validation\Rules;
use App\Models\UsersModel;
use CodeIgniter\HTTP\IncomingRequest;

class UserController extends Controller
{
    public function index()
    {
        return view('UserRegister');
    }

    public function register()
    {
        $validation = Services::validation();

        
        if (!$this->validate([
            'username' => [
                'rules' => 'required|min_length[5]|max_length[255]|is_unique[users.username]|alpha_numeric',
                'errors' => [
                    'required' => 'Username is required.',
                    'min_length' => 'Username must be at least 5 characters long.',
                    'max_length' => 'Username cannot exceed 255 characters.',
                    'alpha_numeric' => 'Username must only contain letters and numbers.',
                    'is_unique' => 'Username already exists.',
                ]
            ],
            'password' => [
                'rules' => 'required|min_length[4]|max_length[255]|regex_match[/(?=.*[a-zA-Z])(?=.*\d)(?=.*[@#$%^&+=!])(?!.*\s).{8,}/]',
                'errors' => [
                    'required' => 'Password is required.',
                    'min_length' => 'Password must be at least 8 characters long.',
                    'max_length' => 'Password cannot exceed 255 characters.',
                    'regex_match' => 'Password must contain at least one letter, one number, and one of the allowed special characters.'

                ]
            ],
            'confirm_password' => [
                'rules' => 'required|matches[password]',
                'errors' => [
                    'required' => 'Confirm password is required.',
                    'matches' => 'Passwords do not match.'
                ]
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }
        $users = new UsersModel();
        $uuid = random_string('md5', 32);
        $data = array(
            'uuid' =>  $uuid,
            'username' => $this->request->getVar('username'),
            'password' => $this->request->getVar('password')

        );
        $users->insert($data);
        // Process successful registration (insert user to database, etc.)
        // ...

        return redirect()->to('/login')->with('success', 'Registration successful!');
    }

    public function loginUI()
    {
        return view('UserLogin');
    }
    
    public function login()
    {
        $throttler = Services::throttler();
        $allowed = $throttler->check('login', 3, MINUTE);

        if ($allowed) {
            $usersModel = new UsersModel();
            $request = request();
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
    
            $user = $usersModel->getUserByUsername($username);
    
            if ($user && password_verify($password, $user['password'])) {
                session()->set('uuid', $user['uuid']);
                return redirect()->to('gallery'); 
            } else {
                session()->setFlashdata('error', 'Invalid username or password.');
                return redirect()->to('login');
            }
        } else {
            session()->setFlashdata('error', 'Too many failed login attempts. Please try again later.');
            return redirect()->back()->withInput();
        }
        
    }

    public function logout()
    {
    session()->destroy();
    return redirect()->to('login');
    }
    
}
