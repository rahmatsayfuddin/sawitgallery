<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\GalleryModel;

class GalleryController extends Controller
{

    public function index($currentPage = 1)
    {
        if (!session()->get('uuid')) {
            // No session, redirect to login
            session()->setFlashdata('error', 'Unauthorized.');
            return redirect()->to(base_url('login'));
        }
        $model = new GalleryModel();
        $limit = 2; // Adjust limit as needed
        $uuid =session()->get('uuid');
        $images = $model->getImages($currentPage, $limit,$uuid);
        $totalImages = $model->countImages();

          // Calculate total number of pages
    $totalPages = ceil($totalImages / $limit);

    // Handle invalid page number (optional)
    if ($currentPage > $totalPages) {
        $currentPage = $totalPages; // Redirect or handle error as needed
    }

        // Calculate start and end index for current page
        $startIndex = ($currentPage - 1) * $limit;
        $endIndex = $startIndex + $limit - 1; // Adjust endIndex if needed for zero-based indexing

        // Pagination links generation (manual approach)
        $paginationLinks = '';
        if ($totalPages > 1) {
            // Previous link (if not on the first page)
            if ($currentPage > 1) {
                $previousPage = $currentPage - 1;
                $paginationLinks .= '<li class="page-item"><a href="' . base_url('gallery/index/' . $previousPage) . '" class="page-link ' . '">Previous </a></li> ';
            }

            // Page links
            for ($i = 1; $i <= $totalPages; $i++) {
                $activeClass = ($i === $currentPage) ? 'active' : '';
                $paginationLinks .= '<li class="page-item"><a href="' . base_url('gallery/index/' . $i) . '" class="page-link ' . $activeClass . '">' . $i . '</a></li>';
            }

            // Next link (if not on the last page)
            if ($currentPage < $totalPages) {
                $nextPage = $currentPage + 1;
                $paginationLinks .= ' <li class="page-item"><a  href="' . base_url('gallery/index/' . $nextPage). '" class="page-link ' . '">Next</a></li>';
            }
        }


        $data = [
            'images' => $images,
            'pagination' => $paginationLinks,
        ];

        return view('Gallery', $data);
    }
    public function upload()
    {
        if (!session()->get('uuid')) {
            // No session, redirect to login
            session()->setFlashdata('error', 'Unauthorized.');
            return redirect()->to(base_url('login'));
        }
        if ($this->request->getMethod() === 'post') {
            // Get file information
            $file = $this->request->getFile('image');

                if (!$this->validate([
                    'image' => [
                        'rules' => 'uploaded[image]|mime_in[image,image/jpg,image/jpeg,image/png]|max_size[image,1024]', // Adjust max size as needed (in KB)
                        'errors' => [
                            'uploaded[image]' => 'Please select an image file.',
                            'mime_in[image]' => 'Only JPG and PNG image formats are allowed.',
                            'max_size[image]' => 'Image size exceeds the maximum limit of {max_size} KB.',
                        ],
                    ],
                    'caption' => 'required',
                ])) {
                    session()->setFlashdata('error', $this->validator->listErrors());
                    return redirect()->back()->withInput();
                }
                else {

                // Get base64-encoded image data
                $mimeType = $file->getClientMimeType();
                $base64Image = base64_encode(file_get_contents($file->getTempName()));
                // Construct the complete data URL
                $dataUrl = "data:".$mimeType. ";base64," . $base64Image;
                // Set data for database
                $data = [
                    'imgid' => random_string('md5', 32),
                    'uuid' => session()->get('uuid'),
                    'caption' => $this->request->getPost('caption'),
                    'base64' => $dataUrl,
                ];

                // Save data to database
                $model = new GalleryModel();
                if ($model->saveImage($data)) {
                    return redirect()->to('gallery'); // Redirect to success page
                } else {
                    // Handle database error
                    echo "Error saving image to database";
                }
            }
        }

        // Show upload form
        return redirect()->to('gallery');
    }

    public function getFullImage($imageId) {
        if (!session()->get('uuid')) {
            // No session, redirect to login
            session()->setFlashdata('error', 'Unauthorized.');
            return redirect()->to(base_url('login'));
        }
        $model = new GalleryModel();
        $uuid = session()->get('uuid');
        $imageData = $model->getImageById($imageId,$uuid);
        if ($imageData) {
            return "<img src=".$imageData->base64.">";
          } else {
            return "no image";
          }
      }

    public function delete($imageId)
    {
        if (!session()->get('uuid')) {
            // No session, redirect to login
            session()->setFlashdata('error', 'Unauthorized.');
            return redirect()->to(base_url('login'));
        }
        $model = new GalleryModel();
        $uuid = session()->get('uuid');
        $deleted = $model->deleteImage($imageId,$uuid);

        if ($deleted) {
            session()->setFlashdata('error', "Image deleted successfully"); 
            return redirect()->to(base_url('gallery'));
        } else {
            session()->setFlashdata('error', "Error Deleting Image");
            return redirect()->back()->withInput();
        }
    }
    
}
